# MAcro Repository for Vim

## About

This is a Vim plugin for saving and re-using macroses. Originally written by [Chamindra de Silva](http://chamindra.googlepages.com/marvim), forked now from [Chamindra's GitHub page](https://github.com/vim-scripts/marvim).

## Changes

Hardcoded mappings were replaced with `<Plug>`-style ones, (in order to enable vim-plug's "lazy" loading), some new commands were added, some redundant options were removed.

## Configuration

To configure, edit your `.vimrc` accordingly:

  - to set a directory for the repository (default is `~/marvim`):

        let g:marvim_store = $VIMHOME . '/macros/'

  - to disable prefix (default is based on the filetype):

        let g:marvim_prefix = 0

  - to set a register for MARVIM to save macroses from (default is `q`):

        let g:marvim_register = 'a' 

  - to set a mapping to search and run a macro from the repository (default is none):

        nmap mr <Plug>MarvimSearch

  - to set a mapping to search and run a macro from the repository over the selection (default is none):

        vmap mr <Plug>MarvimOverSelection

  - to set a mapping to save a macro to the repository (default is none):

        nmap ms <Plug>MarvimStoreM

  - to set a mapping to save a selection as a template to the repository (default is none):

        vmap ms <Plug>MarvimStoreT

  - to set a mapping to put contents of a selected file from the repository to the register `g:marvim_register` (default is none):

        nmap ms <Plug>MarvimToRegister

  - to map a particular saved macro (default is none):

        nmap <silent> M1 <Plug>MarvimSearch insert_macro_name_here<CR>

For other details, please, consider the original [README](https://bitbucket.org/dsjkvf/vim-chamindra-marvim/src/master/README.orig).
