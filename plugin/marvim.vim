
" HEADER
" Description: MARVIM - MAcro Repository for VIM <marvim.vim>
" Author: Chamindra de Silva <chamindra@gmail.com> 
" Maintainer: dsjvkf <dsjkvf@gmail.com>
" Webpage: https://bitbucket.org/dsjkvf/vim-chamindra-marvim
" Notes:
" Change Log:
" -----------
" v0.6 (dsjkvf)
" - running the latest recorded macro from the marvim register over the selection replaced with running a macro from the repository over the selection
" v0.5 (dsjvkf)
" - hardcoded mappings replaced with <Plug> ones
" - options g:marvim_find_key & g:marvim_store_key elimminated (those are now set via mappings in $VIMRC)
" v0.4
" - Now supports autocompletion when searching for macros
" - The filetype gives the default namespace for storage and retrival
" - Refactoring of functions for namespace to directory mappings
" v0.3
" - New namespace features for macro that map to macro sub-directories
" - Supported the creation of macros and templates in a namespace
" - Creates namespace directories if they do not exist
" - Added the ability to point to a different macro store
" - The above permits a shared macro/template repository within a team
" - Refactored code to make macro and template saving a function
" - Fixed bug on windows listing of macros without whole path name
" v0.2
" - Made script windows compatible
" - Makes the macro home directory if it does not exist
" - Recursively looks up base repository subdirectories for macros
" - Creates a macro menu for the GUI versions
" - Changed defauly macro input register to q
" - Made it easy to define configuration details in vimrc
" - Abstracted hotkeys so that they can be defined in the vimrc easily
" - Fixed the breaking on spaces in directory paths
" - Changed template extension to mvt for ease
" - Changed naming conventions to avoid namespace conflict
" v0.1
" - Platform independant script (almost ;-)
" - recording of vim version with macro 
" - redefinition of macro_home and macro_register
" -----------

" SETTINGS

" Check if script is loaded already or if the user does not want it loaded
if exists("g:loaded_marvim")
	finish
endif
let g:loaded_marvim = 1
" Provide menus is possible
if has('menu')
    menu &Macro.&Find :call Marvim_search()<CR>
    nmenu &Macro.&Store\ Macro :call Marvim_macro_store()<CR>
    vmenu &Macro.&Store\ Template y:call Marvim_template_store()<CR>
    nmenu &Macro.&Store\ Template :call Marvim_template_store()<CR>
endif
" Set the marvim repository (platform specific configuration)
if has('win16') || has('win32') || has ('win95') || has('win64')
    let s:macro_home = $HOME.'\marvim\' " under your documents and settings 
    let s:path_seperator = '\'
else " assume UNIX based
    let s:macro_home = $HOME."/.marvim/"
    let s:path_seperator = '/'
endif
" Set the marvim repository explicitly (if defined by user)
if exists('marvim_store')
    if g:marvim_store[-1:] != s:path_seperator
        " add a trailing directory slash if it does not exist  
        let s:macro_home = g:marvim_store.s:path_seperator
    else
        let s:macro_home = g:marvim_store
    endif
endif
" Create the repository directory if it does not exist
if !isdirectory(s:macro_home)
    " need to strip end slash before call the mkdir function
    call mkdir(strpart(s:macro_home, 0 , strlen(s:macro_home)-1), "p")
endif
" Check other possible user defined setings
if !exists('marvim_register')
    let g:marvim_register = 'q'  
endif
if !exists('marvim_prefix')
    let g:marvim_prefix = 1
endif
" Set extensions
let s:vim_ver = strpart(v:version,0,1) " get the major vim version number
let s:ext = '.mv'.s:vim_ver  " specify macro extension by vim version number
let s:text = '.mvt' " template extension

" MAIN

" The input function with a namespace prefix
function! Marvim_input(prompt)
    
    " use a default prefix by filetype unless it has been disabled
    if ( &filetype == '' || !g:marvim_prefix )
        let l:namespace = ''
    else
        let l:namespace = &filetype.':'
    endif

    " get the input with custom autocompletion
    let l:tmp = input(a:prompt, l:namespace, "customlist,Marvim_completion")
    redraw
    return l:tmp

endfunction

" Custom completion function for Marvim_input()
function! Marvim_completion(ArgLead, CmdLine, CursorPos)

    let l:c_list = []  " initialize list
    let l:tmp = tr(a:ArgLead, ":", s:path_seperator)
    let l:search_dir = Marvim_get_directory(s:macro_home.l:tmp)
    let l:search_string = Marvim_get_filename(s:macro_home.l:tmp)

    " recursively search in the namespace directory to get all files
    let l:search_list = glob(l:search_dir.s:path_seperator.'**') 
 
    let l:asearch_list = split(l:search_list, '\n')  

    " filter by the macro extension .mv?
    let l:macro_list = filter(l:asearch_list, 'v:val =~ ".mv"')
 
    " translate each item to the namespace version 
    let l:item_count = 0
    for l:item in l:macro_list

        let l:item_count = l:item_count + 1

        " get the filename
        let l:file_split = strpart(l:item, strlen(s:macro_home))
        let l:filename = tr(l:file_split, s:path_seperator, ":")

        " remove trailing extension TODO: can break on multiple .
        let l:name_split = split (l:filename, '\.') 

        " add the item to the complete list 
        call add(l:c_list, l:name_split[0]) 

    endfor 

    " filter namespace list on postfix search string
    let l:complete_list = filter(l:c_list, 'v:val =~ "'.l:search_string.'"')

    return l:complete_list

endfunction

" Template save in visual mode
function! Marvim_template_store()

    " yank the visual block into the default register
    " if previous command was not visual this is ignored
    " allowing for other forms of yanking
    " silent execute 'normal! `<v`>y<CR>'  

    let l:listtmp = split(@@,'\n') " get default yank buffer
    let l:template_name = Marvim_input('Enter Template Save Name -> ') 

    call Marvim_file_save(l:template_name, l:listtmp, s:text)
    
    echo 'Template '.l:template_name.' saved'

endfunction

" Hotkey mapping dynamic input function for Marvim_file_save
function! Marvim_macro_store()

    let l:macro_name = Marvim_input('Enter Macro Save Name -> ') 

    "let l:listtmp = [@c] " get the macro from register c
    let l:listtmp = [getreg(g:marvim_register)]

    call Marvim_file_save(l:macro_name, l:listtmp, s:ext)
    " clear the command line
    echo 'Macro '.l:macro_name.' Stored'

endfunction

" Get the directory name of the fill file path without last slash
function! Marvim_get_directory(fullfilename)

    let l:lastslash = strridx(a:fullfilename, s:path_seperator)

    let l:dirname = strpart( a:fullfilename, 0, l:lastslash)

    return l:dirname

endfunction

" Get the file name from the directory path 
function! Marvim_get_filename(fullfilename)

    let l:lastslash = strridx(a:fullfilename, s:path_seperator)

    let l:filename = strpart( a:fullfilename, l:lastslash+1)

    return l:filename

endfunction

" Return the fullfilename from the macro name e.g. php:deletebrackets
function! Marvim_ns_to_filename(macro_name)

    let l:name = tr(a:macro_name, ":", s:path_seperator)

    let l:prefix = s:macro_home.l:name

    let l:fullfilename = glob(l:prefix.".mv?")
   
    return l:fullfilename

endfunction

" The macro save function
function! Marvim_file_save(macro_name, data, exttype)

    let l:name = tr(a:macro_name, ":", s:path_seperator)

    let l:fullfilename = s:macro_home.l:name.a:exttype

    let l:dirname = Marvim_get_directory(l:fullfilename)

    if !isdirectory(l:dirname) 

        call mkdir (l:dirname, "p")

    endif

    if ( a:exttype == s:ext ) " if this is a macro

        call writefile(a:data, l:fullfilename, 'b')

    else " else if it a template

        call writefile(a:data, l:fullfilename)
    endif

endfunction

" Run the macro file
" @param macro_file - full path to the macro file
function! Marvim_file_run(macro_file)

    " If the file does not exist or is not readable return
    let l:isMacro = filereadable(a:macro_file)
    if ( l:isMacro == 0 )
        echo 'Macro does not exist'
        return
    endif

    " find if it is a template or a macro
    let l:t = split(a:macro_file, '\.')
    let s:macro_type = l:t[-1] " get the extension

    if s:macro_type == s:text[1:]  " 'mvt' read template 

        silent execute 'read '.a:macro_file

    else  " a vim macro
        " execute 'so! '.s:macro_home.a:macro_name.s:ext
        
        " read the macro file into the register and run it
        let l:macro_content = readfile(a:macro_file,'b')
        call setreg(g:marvim_register,l:macro_content[0]) 
        silent execute 'normal @'.g:marvim_register
        "echo 'Ran Macro '.a:macro_file

    endif

endfunction

" Macro Search function
function! Marvim_search()

    let l:macro_name = Marvim_input('Macro Search -> ')

    " remove leading spaces if present
    " http://stackoverflow.com/a/4479072/1068046
    let l:macro_name = substitute(l:macro_name, '^\s*\(.\{-}\)\s*$', '\1', '')

    if (l:macro_name != '')

        let l:macro_file = Marvim_ns_to_filename(l:macro_name)

        call Marvim_file_run(l:macro_file)

        echo 'Macro '.l:macro_name.' Run'

    endif

endfunction

" Macro Search and run function over selection
function! Marvim_over_selection()
    " select a macro
    let l:macro_name = Marvim_input('Macro Search -> ')
    if (l:macro_name != '')
        let l:macro_file = Marvim_ns_to_filename(l:macro_name)
        " if the file does not exist or is not readable return
        let l:isMacro = filereadable(l:macro_file)
        if ( l:isMacro == 0 )
            echom 'Macro did not exist'
            normal gv
            return
        endif
        " find if it is a template or a macro
        let l:t = split(l:macro_file, '\.')
        let s:macro_type = l:t[-1] " get the extension
        if s:macro_type == s:text[1:]  " if a template was selected ( == mvt)
            echom "Select a macro, not a template"
            normal gv
            return
        else  " if a macro was selected
            " read the macro file into the register and run it
            let l:macro_content = readfile(l:macro_file,'b')
            call setreg(g:marvim_register,l:macro_content[0]) 
        endif
    endif
    execute ":'<,'>normal@" . g:marvim_register
endfunction

" Macro Search and put to register
function! Marvim_to_register()
    " select a macro
    let l:macro_name = Marvim_input('Macro Search -> ')
    if (l:macro_name != '')
        let l:macro_file = Marvim_ns_to_filename(l:macro_name)
        " if the file does not exist or is not readable return
        let l:isMacro = filereadable(l:macro_file)
        if ( l:isMacro == 0 )
            echo 'Macro did not exist'
            return
        endif
        let l:macro_content = readfile(l:macro_file,'b')
        let l:zorro = substitute(l:macro_content[0], "\r", "", "g")
        call setreg(g:marvim_register,l:zorro) 
        echo 'Macro ' . l:macro_file . ' was put under register ' . g:marvim_register . '.'
    endif
endfunction

" MAPPINGS

" Search for a stored macro
nnoremap <Plug>MarvimSearch :call Marvim_search()<CR>
" Store a macro
nnoremap <Plug>MarvimStoreM :call Marvim_macro_store()<CR>
" Store a template
vnoremap <Plug>MarvimStoreT y:call Marvim_template_store()<CR>
" Run the last recorded macro over selection
vnoremap <Plug>MarvimOverSelection :<C-u>call Marvim_over_selection()<CR>
" Place macro (regex) to register
nnoremap <Plug>MarvimToRegister :<C-u>call Marvim_to_register()<CR>
